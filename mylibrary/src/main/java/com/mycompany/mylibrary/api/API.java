package com.mycompany.mylibrary.api;

/**
 * The mighty API
 */
public interface API {
    /**
     * Get a string
     * @return Some string
     */
    String getString();

    /**
     * Get an integer
     * @return Some integer
     */
    int getInt();
}
