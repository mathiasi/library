package com.mycompany.mylibrary;

import com.mycompany.mylibrary.impl.ApiImpl;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class APITest {
    @Test
    public void getString_passes() throws Exception {
        String expected = "myString";
        assertEquals(expected, new ApiImpl().getString());
    }

    @Test
    public void getInt_passes() throws Exception {
        int expected = 1;
        assertEquals(expected, new ApiImpl().getInt());
    }

    /*@Test
    public void shouldFail(){
        assertTrue(false);
    }*/
}