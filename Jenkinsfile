def server
def buildInfoInternal
def buildInfoPublic
def rtGradlePublic
def rtGradleInternal
def shouldDeployPublic
def shouldDeployInternal
// A Declarative Pipeline is defined within a 'pipeline' block.
pipeline {

    // agent defines where the pipeline will run.
    agent {
        // This also could have been 'agent any' - that has the same meaning.
        label ""
        // Other possible built-in agent types are 'agent none', for not running the
        // top-level on any agent (which results in you needing to specify agents on
        // each stage and do explicit checkouts of scm in those stages), 'docker',
        // and 'dockerfile'.
    }

    // The tools directive allows you to automatically install tools configured in
    // Jenkins - note that it doesn't work inside Docker containers currently.
    //tools {
    // Here we have pairs of tool symbols (not all tools have symbols, so if you
    // try to use one from a plugin you've got installed and get an error and the
    // tool isn't listed in the possible values, open a JIRA against that tool!)
    // and installations configured in your Jenkins master's tools configuration.
    //jdk "jdk8"
    // Uh-oh, this is going to cause a validation issue! There's no configured
    // maven tool named "mvn3.3.8"!
    //maven "mvn3.3.8"
    //}

    //environment {
    // Environment variable identifiers need to be both valid bash variable
    // identifiers and valid Groovy variable identifiers. If you use an invalid
    // identifier, you'll get an error at validation time.
    // Right now, you can't do more complicated Groovy expressions or nesting of
    // other env vars in environment variable values, but that will be possible
    // when https://issues.jenkins-ci.org/browse/JENKINS-41748 is merged and
    // released.
    //FOO = "BAR"
    //}

    stages {

        stage('Configuration') {
            steps {
                script {
                    // Obtain an Artifactory server instance, defined in Jenkins --> Manage:
                    server = Artifactory.server SERVER_ID

                    rtGradlePublic = Artifactory.newGradleBuild()
                    rtGradlePublic.tool = GRADLE_TOOL // Tool name from Jenkins configuration
                    rtGradlePublic.resolver repo: 'jcenter', server: server
                    rtGradlePublic.deployer repo: 'public', server: server
                    rtGradlePublic.deployer.deployArtifacts = false
                    // Disable artifacts deployment during Gradle run
                    rtGradlePublic.deployer.deployMavenDescriptors = true
                    rtGradlePublic.usesPlugin = true
                    buildInfoPublic = Artifactory.newBuildInfo()

                    rtGradleInternal = Artifactory.newGradleBuild()
                    rtGradleInternal.tool = GRADLE_TOOL // Tool name from Jenkins configuration
                    rtGradleInternal.resolver repo: 'jcenter', server: server
                    rtGradleInternal.deployer repo: 'internal', server: server
                    rtGradleInternal.deployer.deployArtifacts = false
                    // Disable artifacts deployment during Gradle run
                    rtGradleInternal.deployer.deployMavenDescriptors = true
                    rtGradleInternal.usesPlugin = true
                    buildInfoInternal = Artifactory.newBuildInfo()

                    shouldDeployPublic = BRANCH_NAME ==~ /^(\d+\.)?(\d+\.)?(\*|\d+)$/
                    shouldDeployInternal = BRANCH_NAME ==~ /^(\d+\.)?(\d+\.)?(\*|\d+)(-RC\d?)$/ || shouldDeployPublic
                }
            }
        }

        stage('Build') {
            steps {
                script {
                    rtGradlePublic.run rootDir: '.', buildFile: 'build.gradle', tasks: 'clean build -x test'
                }
            }
        }

        stage('Test') {
            parallel {
                stage('Unit tests') {
                    steps {
                        script {
                            rtGradlePublic.run rootDir: '.', buildFile: 'build.gradle', tasks: 'test'

                            prependJUnitPackageName("pubrelease", pwd() + "/mylibrary/build/test-results/testPubReleaseUnitTest/")
                            prependJUnitPackageName("pubdebug", pwd() + "/mylibrary/build/test-results/testPubDebugUnitTest/")
                            prependJUnitPackageName("internaldebug", pwd() + "/mylibrary/build/test-results/testInternalDebugUnitTest/")
                            prependJUnitPackageName("internalrelease", pwd() + "/mylibrary/build/test-results/testInternalReleaseUnitTest/")
                            junit '**/test-results/test*UnitTest/TEST-*.xml'
                        }
                    }
                }
                stage('Instrumented') {
                    steps {
                        script {
                            rtGradlePublic.run rootDir: '.', buildFile: 'build.gradle', tasks: 'connectedAndroidTest'
                        }
                    }
                }
            }
        }

        //Todo Jacoco test coverage?
        stage('Check code coverage') {
            steps{
                script {
                    rtGradlePublic.run rootDir: '.', buildFile: 'build.gradle', tasks: 'jacocoTestReport'
                }
                jacoco buildOverBuild: true, changeBuildStatus: true, classPattern: '**/intermediates/classes', deltaLineCoverage: '60', exclusionPattern: '**/R.class,**/R$*.class,android/**/*.*,**/BuildConfig.*', minimumLineCoverage: '80'
            }
        }

        //Todo sonarQube analysis?

        stage('Deploy') {
            parallel {
                stage('Public') {
                    when {
                        expression {
                            return shouldDeployPublic
                        }
                    }
                    steps {
                        script {
                            rtGradlePublic.run rootDir: '.', buildFile: 'build.gradle', tasks: 'artifactoryPublish -Dmode=public', buildInfo: buildInfoPublic
                            rtGradlePublic.deployer.deployArtifacts buildInfoPublic
                        }
                    }
                }
                stage('Internal') {
                    when {
                        expression {
                            return shouldDeployInternal
                        }
                    }
                    steps {
                        script {
                            rtGradleInternal.run rootDir: '.', buildFile: 'build.gradle', tasks: 'artifactoryPublish -Dmode=internal', buildInfo: buildInfoInternal

                            rtGradleInternal.deployer.deployArtifacts buildInfoInternal
                        }
                    }
                }
            }
        }


        stage('Publish build info') {
            parallel {
                stage('Public') {
                    when {
                        expression {
                            return shouldDeployPublic
                        }
                    }
                    steps {
                        script {
                            server.publishBuildInfo buildInfoPublic
                        }
                    }
                }
                stage('Internal') {
                    when {
                        expression {
                            return shouldDeployInternal
                        }
                    }
                    steps {
                        script {
                            server.publishBuildInfo buildInfoInternal
                        }
                    }
                }
            }
        }
    }

    post {
        // Always runs. And it runs before any of the other post conditions.
        always {
            // Let's wipe out the workspace before we finish!
            deleteDir()
        }

        /*success {
            mail(from: "bob@example.com",
                    to: "steve@example.com",
                    subject: "That build passed.",
                    body: "Nothing to see here")
        }*/

        failure {
            /*mail(from: "bob@example.com",
                    to: "steve@example.com",
                    subject: "That build failed!",
                    body: "Nothing to see here")*/
            emailext attachLog: true, body: "${env.BUILD_URL}", recipientProviders: [[$class: 'CulpritsRecipientProvider']], subject: "Build failed for ${currentBuild.fullDisplayName}"
        }
    }

    // The options directive is for configuration that applies to the whole job.
    options {
        // For example, we'd like to make sure we only keep 10 builds at a time, so
        // we don't fill up our storage!
        //buildDiscarder(logRotator(numToKeepStr:'10'))

        // And we'd really like to be sure that this build doesn't hang forever, so
        // let's time it out after an hour.
        timeout(time: 60, unit: 'MINUTES')
    }
}

def prependJUnitPackageName(prefix, path) {
    sh "find ${path}/ -type f -exec sed -i \"s/\\(classname=['\\\"]\\)/\\1${prefix}./g\" {} \\;"
}